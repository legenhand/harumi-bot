FROM python:3.10.4-bullseye

# clone git repo harumi bot
RUN git clone https://gitlab.com/legenhand/harumi-bot.git /root/harumi

#Copy config file
COPY ./example.config.json ./config.json* /root/harumi/
WORKDIR /root/harumi

# Install requirements
RUN pip3 install -U -r requirements.txt

CMD python3 -m harumi